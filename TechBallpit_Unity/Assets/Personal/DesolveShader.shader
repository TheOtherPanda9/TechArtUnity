// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DesolveShader"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 1
		_Width("Width", Float) = 0
		_Amount("Amount", Float) = 0
		_Boost("Boost", Float) = 0
		_seamless_fire_texture_by_hhh316_d5isg37fullview("seamless_fire_texture_by_hhh316_d5isg37-fullview", 2D) = "white" {}
		_Color0("Color 0", Color) = (0,0,0,0)
		_Color1("Color 1", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color1;
		uniform float _Amount;
		uniform float _Width;
		uniform sampler2D _seamless_fire_texture_by_hhh316_d5isg37fullview;
		uniform float4 _seamless_fire_texture_by_hhh316_d5isg37fullview_ST;
		uniform float4 _Color0;
		uniform float _Boost;
		uniform float _Cutoff = 1;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Albedo = _Color1.rgb;
			float temp_output_6_0_g6 = _Amount;
			float ifLocalVar13_g6 = 0;
			if( temp_output_6_0_g6 <= 0.0 )
				ifLocalVar13_g6 = 0.0;
			else
				ifLocalVar13_g6 = ( temp_output_6_0_g6 + _Width );
			float2 uv_seamless_fire_texture_by_hhh316_d5isg37fullview = i.uv_texcoord * _seamless_fire_texture_by_hhh316_d5isg37fullview_ST.xy + _seamless_fire_texture_by_hhh316_d5isg37fullview_ST.zw;
			float4 tex2DNode24 = tex2D( _seamless_fire_texture_by_hhh316_d5isg37fullview, uv_seamless_fire_texture_by_hhh316_d5isg37fullview );
			float2 temp_cast_2 = (tex2DNode24.a).xx;
			float simplePerlin2D25 = snoise( temp_cast_2 );
			simplePerlin2D25 = simplePerlin2D25*0.5 + 0.5;
			float simplePerlin2D23 = snoise( tex2DNode24.rg*simplePerlin2D25 );
			simplePerlin2D23 = simplePerlin2D23*0.5 + 0.5;
			float temp_output_7_0_g6 = simplePerlin2D23;
			float3 _Vector0 = float3(0,0,0);
			float4 ifLocalVar11_g6 = 0;
			if( ifLocalVar13_g6 <= temp_output_7_0_g6 )
				ifLocalVar11_g6 = float4( _Vector0 , 0.0 );
			else
				ifLocalVar11_g6 = ( _Color0 * _Boost );
			o.Emission = ifLocalVar11_g6.rgb;
			o.Alpha = 1;
			float ifLocalVar16_g6 = 0;
			if( temp_output_6_0_g6 >= temp_output_7_0_g6 )
				ifLocalVar16_g6 = 0.0;
			else
				ifLocalVar16_g6 = 1.0;
			float ifLocalVar22_g6 = 0;
			if( temp_output_6_0_g6 <= 0.0 )
				ifLocalVar22_g6 = 1.0;
			else
				ifLocalVar22_g6 = ifLocalVar16_g6;
			clip( ifLocalVar22_g6 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18712
1920;6;1920;1013;1422;503.5;1;True;True
Node;AmplifyShaderEditor.SamplerNode;24;-1577,-220.5;Inherit;True;Property;_seamless_fire_texture_by_hhh316_d5isg37fullview;seamless_fire_texture_by_hhh316_d5isg37-fullview;5;0;Create;True;0;0;0;False;0;False;-1;44c0c2fe69e88b54a9097a5937cc3d25;44c0c2fe69e88b54a9097a5937cc3d25;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;25;-1350,113.5;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-649,-241.5;Inherit;False;Property;_Width;Width;1;0;Create;True;0;0;0;False;0;False;0;0.07;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;12;-857,189.5;Inherit;False;Property;_Boost;Boost;4;0;Create;True;0;0;0;False;0;False;0;20;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-743,-144.5;Inherit;False;Property;_Amount;Amount;2;0;Create;True;0;0;0;False;0;False;0;0.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;23;-1048,11.5;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;26;-1053,-330.5;Inherit;False;Property;_Color0;Color 0;6;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,0.8835709,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;20;-466,-63.5;Inherit;False;DesolveShaderFunction;-1;;6;820f403c861aefc46943e5bab1e01e40;0;5;4;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;COLOR;0,0,0,0;False;9;FLOAT;0;False;2;COLOR;29;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-995,-147.5;Inherit;False;Property;_Color;Color;3;0;Create;True;0;0;0;False;0;False;0;1.17;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;27;-692,-446.5;Inherit;False;Property;_Color1;Color 1;7;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,0.8835709,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;94,-149;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;DesolveShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;1;True;True;0;False;TransparentCutout;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;25;0;24;4
WireConnection;23;0;24;0
WireConnection;23;1;25;0
WireConnection;20;4;8;0
WireConnection;20;6;10;0
WireConnection;20;7;23;0
WireConnection;20;8;26;0
WireConnection;20;9;12;0
WireConnection;0;0;27;0
WireConnection;0;2;20;29
WireConnection;0;10;20;0
ASEEND*/
//CHKSM=472F47861AF98EAE4C3902C8696FF58239FA4905